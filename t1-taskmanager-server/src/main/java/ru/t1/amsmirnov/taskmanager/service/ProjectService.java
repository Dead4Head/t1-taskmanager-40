package ru.t1.amsmirnov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IProjectService;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.NameEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project add(@Nullable final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (final Exception exception) {
            sqlSession.rollback();
            throw exception;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(
            @Nullable final String userId,
            @Nullable final Project project
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Collection<Project> addAll(@Nullable final Collection<Project> projects) throws AbstractException {
        if (projects == null) throw new ModelNotFoundException(Project.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            for (Project project : projects) repository.add(project);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return projects;
    }

    @NotNull
    @Override
    public Collection<Project> set(@Nullable final Collection<Project> projects) throws AbstractException {
        if (projects == null) throw new ModelNotFoundException(Project.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
        try {
            repository.removeAll();
            for (Project project : projects) repository.add(project);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws AbstractException {
        @Nullable final List<Project> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            projects = repository.findAll();
            if (projects == null)
                return Collections.emptyList();
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) throws AbstractException {
        @Nullable final List<Project> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            projects = repository.findAllSorted(getComparator(comparator));
            if (projects == null)
                return Collections.emptyList();
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Project> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            projects = repository.findAllUserId(userId);
            if (projects == null)
                return Collections.emptyList();
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<Project> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Project> projects;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            projects = repository.findAllSortedUserId(userId, getComparator(comparator));
            if (projects == null)
                return Collections.emptyList();
        }
        return projects;
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(id);
        }
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Project project;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneByIdUserId(userId, id);
        }
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project update(@Nullable final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return update(project);
    }


    @NotNull
    @Override
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        if (status == null) return project;
        project.setStatus(status);
        return update(project);
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeAllUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            for (final Project project : collection)
                repository.removeOneByIdUserId(project.getUserId(), project.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project removeOne(@Nullable final Project project) throws AbstractException {
        if (project == null) throw new ProjectNotFoundException();
        return removeOneById(project.getId());
    }

    @NotNull
    @Override
    public Project removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        final Project project;
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(id);
            repository.removeOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project removeOne(
            @Nullable final String userId,
            @Nullable final Project project
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ModelNotFoundException("Project");
        return removeOneById(userId, project.getId());
    }

    @NotNull
    @Override
    public Project removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(id);
            repository.removeOneByIdUserId(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getSize();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getSizeUserId(userId);
        }
    }

    @Override
    public boolean existById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existById(id);
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.existByIdUserId(userId, id);
        }
    }

    private String getComparator(Comparator<?> comparator) {
        if (comparator instanceof StatusComparator) return "status";
        if (comparator instanceof NameComparator) return "name";
        return "created";
    }

}