package ru.t1.amsmirnov.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, created, name, description, status, user_id)" +
            " VALUES (#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})")
    void add(@NotNull Project project);

    @Nullable
    @Select("SELECT * FROM tm_project ORDER BY created")
    @Result(property = "userId", column = "user_id")
    List<Project> findAll();

    @Nullable
    @Select("SELECT * FROM tm_project ORDER BY #{column}")
    @Result(property = "userId", column = "user_id")
    List<Project> findAllSorted(@NotNull @Param("column") String column);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY created")
    @Result(property = "userId", column = "user_id")
    List<Project> findAllUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{column}")
    @Result(property = "userId", column = "user_id")
    List<Project> findAllSortedUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("column") String column
    );

    @Select("SELECT * FROM tm_project WHERE id = #{id}")
    @Result(property = "userId", column = "user_id")
    Project findOneById(
            @NotNull @Param("id") String id
    );

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Result(property = "userId", column = "user_id")
    Project findOneByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT EXISTS(SELECT 1 FROM tm_project WHERE id = #{id})")
    boolean existById(
            @NotNull @Param("id") String id
    );

    @Select("SELECT EXISTS(SELECT 1 FROM tm_project WHERE user_id = #{userId} AND id = #{id})")
    boolean existByIdUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT COUNT(1) FROM tm_project")
    int getSize();

    @Select("SELECT COUNT(1) FROM tm_project WHERE user_id = #{userId}")
    int getSizeUserId(@NotNull @Param("userId") String userId);

    @Update("UPDATE tm_project SET created = #{created}," +
            " name = #{name}," +
            " description = #{description}," +
            " status = #{status}," +
            " user_id = #{userId}" +
            " WHERE id = #{id}")
    void update(@NotNull Project project);

    @Delete("DELETE FROM tm_project")
    void removeAll();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void removeAllUserId(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE id = #{id}")
    void removeOneById(
            @Param("id") @NotNull String id
    );

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeOneByIdUserId(
            @Param("userId") @NotNull String userId,
            @Param("id") @NotNull String id
    );

}
