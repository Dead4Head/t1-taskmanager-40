package ru.t1.amsmirnov.taskmanager.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.ISessionService;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.IdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.amsmirnov.taskmanager.model.Session;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Session add(@Nullable final Session session) throws AbstractException {
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.add(session);
            sqlSession.commit();
        } catch (final Exception exception) {
            sqlSession.rollback();
            throw exception;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(
            @Nullable final String userId,
            @Nullable final Session session
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        session.setUserId(userId);
        return add(session);
    }

    @NotNull
    @Override
    public Collection<Session> addAll(@Nullable final Collection<Session> sessions) throws AbstractException {
        if (sessions == null) throw new ModelNotFoundException(Session.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            for (Session session : sessions) repository.add(session);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public Collection<Session> set(@Nullable final Collection<Session> sessions) throws AbstractException {
        if (sessions == null) throw new ModelNotFoundException(Session.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeAll();
            for (Session session : sessions) repository.add(session);
            sqlSession.commit();
        } catch (final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return sessions;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws AbstractException {
        @Nullable final List<Session> sessions;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            sessions = repository.findAll();
            if (sessions == null)
                return Collections.emptyList();
        }
        return sessions;
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final Comparator<Session> comparator) throws AbstractException {
        return findAll();
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<Session> sessions;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            sessions = repository.findAllUserId(userId);
            if (sessions == null)
                return Collections.emptyList();
        }
        return sessions;
    }

    @NotNull
    @Override
    public List<Session> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<Session> comparator
    ) throws AbstractException {
        return findAll(userId);
    }

    @NotNull
    @Override
    public Session findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneById(id);
        }
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        return session;
    }

    @NotNull
    @Override
    public Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session;
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneByIdUserId(userId, id);
        }
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        return session;
    }

    @NotNull
    @Override
    public Session update(@Nullable final Session session) throws AbstractException {
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.update(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    public void removeAll() {
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws AbstractException {
        if (userId == null) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeAllUserId(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<Session> collection) {
        if (collection == null) return;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            for (final Session session : collection)
                repository.removeOneByIdUserId(session.getUserId(), session.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Session removeOne(@Nullable final Session session) throws AbstractException {
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        return removeOneById(session.getId());
    }

    @NotNull
    @Override
    public Session removeOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        final Session session;
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneById(id);
            repository.removeOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        return session;
    }

    @NotNull
    @Override
    public Session removeOne(
            @Nullable final String userId,
            @Nullable final Session session
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        return removeOneById(userId, session.getId());
    }

    @NotNull
    @Override
    public Session removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Session session;
        @NotNull final SqlSession sqlSession = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            session = repository.findOneById(id);
            repository.removeOneByIdUserId(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        if (session == null) throw new ModelNotFoundException(Session.class.getName());
        return session;
    }

    @Override
    public int getSize() {
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.getSize();
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.getSizeUserId(userId);
        }
    }

    @Override
    public boolean existById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.existById(id);
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            return repository.existByIdUserId(userId, id);
        }
    }

}
