package ru.t1.amsmirnov.taskmanager.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

import java.util.Date;

public final class Session extends AbstractUserOwnedModel {

    @NotNull
    private Date created = new Date();

    @Nullable
    private Role role = Role.USUAL;

    @NotNull
    public Date getCreated() {
        return created;
    }

    public void setCreated(@NotNull final Date created) {
        this.created = created;
    }

    @Nullable
    public Role getRole() {
        return role;
    }

    public void setRole(@Nullable final Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return getId() + " " + getCreated();
    }
}
